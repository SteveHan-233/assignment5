#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

# -----------
# imports
# -----------

import json
import random

from flask import (Flask, Response, jsonify, redirect, render_template,
                   request, url_for)
import itertools

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'},
         {'title': 'Algorithm Design', 'id': '2'},
         {'title': 'Python', 'id': '3'}]


def addBook(title):
    for i in itertools.count(1):
        found = False
        for book in books:
            if (book["id"] == str(i)):
                found = True
                break
        if not found:
            new_id = str(i)
            break
    books.append({'title': title, 'id': new_id})


def getBook(id_):
    print(id_)
    return next(filter(lambda book: book['id'] == str(id_), books))


@app.route('/book/JSON/')
def bookJSON():
    return Response(json.dumps(books))


@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('index.html', books=books)


@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        addBook(request.form['title'])
        return redirect(url_for('showBook'))
    else:
        return render_template('newbook.html')


@app.route('/book/<int:book_id>/edit/', methods=['GET', 'POST'])
def editBook(book_id):
    if request.method == 'POST':
        getBook(book_id)["title"] = request.form['title']
        return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html', old_title=getBook(book_id)["title"], id=book_id)


@app.route('/book/<int:book_id>/delete/', methods=['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        books.remove(getBook(book_id))
        return redirect(url_for('showBook'))
    else:
        return render_template('deletebook.html', title=getBook(book_id)["title"], id=book_id)


if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
